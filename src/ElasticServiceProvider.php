<?php

namespace Centralpos\ElasticScout;

use ElasticScoutDriver\Engine;
use ElasticScoutDriverPlus\Decorators\EngineDecorator;
use Illuminate\Support\ServiceProvider;
use ElasticMigrations\ServiceProvider as MigrationsProvider;
use ElasticClient\ServiceProvider as ClientProvider;
use ElasticScoutDriver\ServiceProvider as ElasticScoutProvider;
use Laravel\Scout\Engines\Engine as ScoutEngine;
use Laravel\Scout\ScoutServiceProvider;

class ElasticServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app['config']->set(
            'elastic.migrations.index_name_prefix',
            env('SCOUT_PREFIX', '')
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->instance('path.config', config_path());

        $this->registerWithBindings(ScoutServiceProvider::class);
        $this->registerWithBindings(ClientProvider::class);
        $this->registerWithBindings(ElasticScoutProvider::class);
        $this->extendEngine();

        if ($this->app->runningInConsole()) {
            $this->registerWithBindings(MigrationsProvider::class);
        }
    }

    protected function registerWithBindings($provider)
    {
        $this->app->register($provider);

        if (property_exists($provider, 'bindings')) {
            $bindings = (new $provider($this->app))->bindings;

            foreach ($bindings as $abstract => $concrete) {
                $this->app->bind($abstract, $concrete);
            }
        }
    }

    protected function extendEngine()
    {
        $this->app->extend(Engine::class, function (ScoutEngine $engine) {
            return $this->app->make(EngineDecorator::class, compact('engine'));
        });
    }
}
