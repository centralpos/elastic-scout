<?php


namespace Centralpos\ElasticScout;


use ElasticScoutDriverPlus\Builders\QueryBuilderInterface;
use ElasticScoutDriverPlus\Builders\SearchRequestBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Traits\ForwardsCalls;

class SearchBuilder
{
    use ForwardsCalls;

    /**
     * @var \Illuminate\Database\Eloquent\Model|SearchableModel
     */
    private $model;
    /**
     * @var QueryBuilderInterface
     */
    private $queryBuilder;
    /**
     * @var SearchRequestBuilder
     */
    private $searchRequestBuilder;

    /**
     * SearchBuilder constructor.
     * @param  Model|SearchableModel $model
     * @param  QueryBuilderInterface $queryBuilder
     */
    public function __construct(Model $model, QueryBuilderInterface $queryBuilder)
    {
        $this->model = $model;
        $this->queryBuilder = $queryBuilder;
        $this->searchRequestBuilder = new SearchRequestBuilder($model, $queryBuilder);
    }

    /**
     * @param  int|null $perPage
     * @param  string $pageName
     * @param  int|null $page
     * @return LengthAwarePaginator
     */
    public function paginate(int $perPage = null, string $pageName = 'page', int $page = null)
    {
        $page = $page ?: Paginator::resolveCurrentPage($pageName);

        $perPage = $perPage ?: $this->model->getPerPage();

        $this->searchRequestBuilder->from(($page-1)*$perPage);
        $this->searchRequestBuilder->size($perPage);

        $searchResult = $this->searchRequestBuilder->execute();
        $items = $this->model->searchResultFormat() == 'models'
            ? $searchResult->models() : $searchResult->documents();

        if ($this->model->searchResultFormat() == 'array') {
            $items = $this->documentsToArray($items);
        }

        $paginator = (new LengthAwarePaginator($items, $searchResult->total(), $perPage, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => $pageName,
        ]));

        return $paginator;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    public function get()
    {
        $searchResult = $this->searchRequestBuilder->execute();

        $items = $this->model->searchResultFormat() == 'models'
            ? $searchResult->models() : $searchResult->documents();

        if ($this->model->searchResultFormat() == 'array') {
            $items = collect($this->documentsToArray($items));
        }

        return $items;
    }

    /**
     * @param  \Illuminate\Support\Collection|\ElasticAdapter\Documents\Document[] $documents
     * @return array
     */
    protected function documentsToArray($documents): array
    {
        $data = [];

        foreach ($documents as $document) {
            $data[] = $document->getContent();
        }

        return $data;
    }

    /**
     * @param  string $method
     * @param  array $parameters
     * @return mixed
     */
    public function __call(string $method, array $parameters)
    {
        $response = $this->forwardCallTo($this->searchRequestBuilder, $method, $parameters);

        return $response instanceof SearchRequestBuilder
            ? $this : $response;
    }
}
