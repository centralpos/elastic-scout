<?php


namespace Centralpos\ElasticScout;

use ElasticScoutDriverPlus\Builders\BoolQueryBuilder;
use ElasticScoutDriverPlus\Builders\SearchRequestBuilder;
use Illuminate\Http\Request;
use \Laravel\Scout\Searchable as Scout;

trait SearchableModel
{
    use Scout;

    /**
     * @param  Request $request
     * @return SearchRequestBuilder&BoolQueryBuilder&SearchBuilder
     * @throws \Exception
     */
    public static function searchFromRequest(Request $request)
    {
        return ElasticParser::fromArray(
            new SearchBuilder(new static, new BoolQueryBuilder()),
            $request->all()
        );
    }

    /**
     * @return SearchRequestBuilder&BoolQueryBuilder&SearchBuilder
     */
    public static function search($query = '')
    {
        $builder = new SearchBuilder(new static, new BoolQueryBuilder());

        if ($query) {
            $builder->must('query_string', compact('query'));
        }

        return $builder;
    }

    /**
     * @return string
     */
    public function searchResultFormat()
    {
        return 'models';
    }
}
