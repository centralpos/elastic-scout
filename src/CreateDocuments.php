<?php


namespace Centralpos\ElasticScout;


use ElasticAdapter\Documents\Document;
use ElasticAdapter\Documents\DocumentManager;
use Elasticsearch\ClientBuilder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

trait CreateDocuments
{
    /**
     * @param  Collection|Model[] $models
     * @return array|Document[]
     */
    protected function makeDocuments(Collection $models): array
    {
        $documents = [];

        foreach ($models as $model) {
            $content = $this->formatDocumentContent($model);

            $documents[] = new Document(
                $this->documentKey($model),
                $content
            );
        }

        return $documents;
    }

    /**
     * @param Builder $query
     * @param string $index
     * @param string $orderByColumn
     * @param string $orderByDirection
     */
    protected function updateIndexFromQuery(Builder $query, string $index, string $orderByColumn, string $orderByDirection = 'asc')
    {
        $manager = $this->manager();

        $query->orderBy($orderByColumn, $orderByDirection);

        $query->chunk($this->chunkSize(), function($models) use ($manager, $index) {
            $manager->index(
                $index, $this->makeDocuments($models)
            );
        });
    }

    /**
     * @param  Model $model
     * @return mixed
     */
    protected function documentKey(Model $model)
    {
        return $model->getKey();
    }

    /**
     * @param  Model $model
     * @return array
     */
    protected function formatDocumentContent(Model $model): array
    {
        return $model->toArray();
    }

    /**
     * @return int
     */
    protected function chunkSize(): int
    {
        return 500;
    }

    /**
     * @return DocumentManager
     */
    protected function manager(): DocumentManager
    {
        $client = ClientBuilder::fromConfig([
            'hosts' => [
                env('ELASTIC_HOST', 'localhost:9200'),
            ]
        ]);
        return new DocumentManager($client);
    }
}
