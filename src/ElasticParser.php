<?php


namespace Centralpos\ElasticScout;


use ElasticScoutDriverPlus\Builders\BoolQueryBuilder;
use ElasticScoutDriverPlus\Builders\SearchRequestBuilder;
use Exception;

class ElasticParser
{
    /**
     * @param  SearchBuilder $builder
     * @param  array $params
     * @return SearchRequestBuilder&BoolQueryBuilder&SearchBuilder
     * @throws \Exception
     */
    public static function fromArray(SearchBuilder $builder, array $params)
    {
        if (isset($params['q']) && $query = $params['q']) {
            $builder->must('query_string', compact('query'));
            unset($params['q']);
        }

        $filters = self::parseFilters($params);

        foreach ($filters as $type => $queries) {
            foreach ($queries as $field => $query) {
                $builder->filter($type, [$field => $query]);
            }
        }

        return $builder;
    }

    /**
     * @param  array $params
     * @return array
     * @throws Exception
     */
    public static function parseFilters(array $params)
    {
        $filter = [];

        foreach (self::groupFilters($params) as $type => $queries) {
            $method = $type."FiltersParser";
            $filter[$type] = self::$method($queries);
        }

        return $filter;
    }

    /**
     * @param  array $params
     * @return array
     * @throws Exception
     */
    protected static function groupFilters($params)
    {
        $grouped = [];

        $filterTypes = [
            'term' => ['eq'],
            'terms' => ['in'],
            'range' => [
                'gt' => 'gt',
                'gte' => 'min',
                'lt' => 'st',
                'lte' => 'max'
            ],
        ];

        foreach ($params as $param => $val) {

            $filterType = null;
            $paramOperator = explode('-', $param);
            $field = $paramOperator[0];
            $operator = isset($paramOperator[1]) ? $paramOperator[1] : 'eq';

            foreach ($filterTypes as $type => $operators) {
                if (in_array($operator, $operators)) {

                    $filterType = $type;
                    $data = [$field, $operator, $val];
                    break;
                }
            }

            if (!$filterType) {
                throw new Exception("Unsupported param $param");
            }

            if (!isset($grouped[$filterType])) {
                $grouped[$filterType] = [];
            }

            $grouped[$filterType][] = $data;
        }

        return $grouped;
    }

    /**
     * @param  array $termsFilters
     * @return array
     */
    protected static function termsFiltersParser($termsFilters)
    {
        $formatted = [];

        foreach ($termsFilters as $filter) {
            list($field, $operator, $val) = $filter;
            $val = explode(',', $val);

            $formatted[$field] = $val;
        }

        return $formatted;
    }

    /**
     * @param  array $termFilters
     * @return array
     */
    protected static function termFiltersParser($termFilters)
    {
        $formatted = [];

        foreach ($termFilters as $filter) {
            list($field, $operator, $val) = $filter;
            $formatted[$field] = $val;
        }

        return $formatted;
    }

    /**
     * @param  array $rangeFilters
     * @return array
     */
    protected static function rangeFiltersParser($rangeFilters)
    {
        $formatted = [];
        $filters = [];

        $elaOperators = [
            'gt' => 'gt',
            'min' => 'gte',
            'st' => 'lt',
            'max' => 'lte'
        ];

        foreach ($rangeFilters as $rangeFilterilter) {
            list($field, $operator, $val) = $rangeFilterilter;
            $operator = $elaOperators[$operator];

            if (!isset($filters[$field])) {
                $filters[$field] = [];
            }

            $filters[$field][$operator] = $val;
        }

        foreach ($filters as $field => $filter) {
            $formatted[$field] = $filter;
        }

        return $formatted;
    }
}
